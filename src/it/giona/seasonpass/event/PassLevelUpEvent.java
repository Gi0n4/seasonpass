package it.giona.seasonpass.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PassLevelUpEvent extends Event implements Cancellable {

    private static final HandlerList HANDLERS = new HandlerList();

    private Player p;
    private int level;

    private boolean isCancelled;

    public PassLevelUpEvent(Player p, int level){
        this.isCancelled = false;
        this.p = p;
        this.level = level;
    }

    public Player getPlayer(){
        return this.p;
    }

    public int getNewLevel(){
        return this.level;
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        isCancelled = cancel;
    }

    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
