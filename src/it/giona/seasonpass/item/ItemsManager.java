package it.giona.seasonpass.item;

import it.giona.seasonpass.Main;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class ItemsManager {

    private Main plugin;
    private List<ItemPack> packs;

    public ItemsManager(Main p){
        this.plugin = p;
        this.packs = new ArrayList<>();
    }

    public void updateItemPacks(){
        packs.clear();
        for(String id : plugin.getItems().getConfig().getConfigurationSection("Items").getKeys(false)){
            String material = plugin.getItems().getConfig().getString("Items." + id + ".Material");
            try{
                Material m = Material.valueOf(material);
                int amount = plugin.getItems().getConfig().getInt("Items." + id + ".Amount", 1);
                int data = plugin.getItems().getConfig().getInt("Items." + id + ".Data", 0);

                ItemStack item = new ItemStack(m, amount, (byte)data);
                ItemMeta meta = item.getItemMeta();

                String name = plugin.tacc(plugin.getItems().getConfig().getString("Items." + id + ".Name", null));
                meta.setDisplayName(name);

                List<String> lore = plugin.taccList(plugin.getItems().getConfig().getStringList("Items." + id + ".Lore"));
                meta.setLore(lore);

                if(plugin.getItems().getConfig().getBoolean("Items." + id + ".hide-enchantments")){
                    meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                }

                item.setItemMeta(meta);

                List<String> enchants = plugin.getItems().getConfig().getStringList("Items." + id + ".Enchants");
                for(String e : enchants){
                    Enchantment en = Enchantment.getByName(e.split(":")[0]);
                    if(en == null){
                        plugin.getLogger().info("Enchant non valido sull'itempack " + id);
                        continue;
                    }
                    int level = Integer.valueOf(e.split(":")[1]);
                    item.addUnsafeEnchantment(en, level);
                }

                packs.add(new ItemPack(id, item));
            } catch (IllegalArgumentException ex){
                plugin.getLogger().info("Impossibile riconoscere la configurazione dell'item " + id);
            }
        }
    }

    public ItemPack getItemPack(String identifier){
        for(ItemPack ip : packs){
            if(ip.getIdentifier().equals(identifier)){
                return ip;
            }
        }
        return null;
    }

    public boolean isItemPack(String identifier){
        return getItemPack(identifier) != null;
    }
}
