package it.giona.seasonpass.papi;

import it.giona.seasonpass.Main;
import it.giona.seasonpass.seasonpass.PassLevel;
import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class PAPIHook extends PlaceholderExpansion {

    private Main plugin;

    public PAPIHook(Main p) {
        this.plugin = p;
        register();
    }

    @Override
    public String getIdentifier() {
        return "sp";
    }

    @Override
    public String getAuthor() {
        return "Gi0n4 & RhalisX99";
    }

    @Override
    public String getVersion() {
        return Main.getInstance().getDescription().getVersion();
    }

    @Override
    public String onPlaceholderRequest(Player p, String identifier) {

        if (p == null) {
            return "";
        }

        PassLevel next = plugin.getPassManager().getLevel(plugin.getPlayerData().getPassLevel(p) + 1);
        DecimalFormat decimalFormat = new DecimalFormat("0.0");
        switch(identifier.toLowerCase()) {
            case "player_level":
                if(plugin.getPlayerData().getPassLevel(p) < plugin.getPassManager().maxLevel){
                    return String.valueOf(plugin.getPlayerData().getPassLevel(p));
                }else{
                    return plugin.getLang().getConfig().getString("Messages.MaxLevel.Scoreboard.Ended").replace("&", "§");
                }
            case "player_stars":
                if(plugin.getPlayerData().getPassLevel(p) < plugin.getPassManager().maxLevel){
                    return Main.getInstance().getPlayerData().getStars(p)+"/"+next.getPrice()+" §e§l✫";
                }else{
                    return "§a§l✔";
                }
            case "player_stars_needed":
                if(plugin.getPlayerData().getPassLevel(p) < plugin.getPassManager().maxLevel){
                    int needed = next.getPrice() - Main.getInstance().getPlayerData().getStars(p);
                    if(needed > 0){
                        return String.valueOf(needed);
                    }
                    return plugin.getLang().getConfig().getString("Items.Status.Waiting");
                }else{
                    return plugin.getLang().getConfig().getString("Items.Status.Completed");
                }
            case "player_pass_progress":
                double perc1 = plugin.getPlayerData().getPassLevel(p) * 100 / plugin.getPassManager().maxLevel;
                if(perc1> 100){
                    return "100%";
                }else {
                    return decimalFormat.format(perc1).replace(",", ".") + "%";
                }
            case "player_level_progress":
                if(plugin.getPlayerData().getPassLevel(p) == plugin.getPassManager().maxLevel) {
                        return plugin.getLang().getConfig().getString("Items.Status.Completed");
                }else {
                    double perc = ((Main.getInstance().getPlayerData().getStars(p) * 100)/next.getPrice());
                    if (perc > 100) {
                        return "100%";
                    } else {
                        return decimalFormat.format(perc).replace(",", ".") + "%";
                    }
                }
            case "get_week_count":
                return plugin.getDateManager().getCountdown();

            case "get_week_name":
                return plugin.getDateManager().getNextEventName();
        }
        return null;
    }

    @Override
    public boolean register() {
        if(canRegister()){
            PlaceholderAPI.registerExpansion(this);
            return true;
        }
        return false;
    }

    @Override
    public boolean canRegister() {
        return plugin.getServer().getPluginManager().isPluginEnabled(getPlugin());
    }

    @Override
    public String getPlugin(){
        return "seasonpass";
    }

}
