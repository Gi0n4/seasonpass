package it.giona.seasonpass.papi;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;
import it.giona.seasonpass.Main;
import org.bukkit.entity.Player;

public class MVDWHook {

    private Main plugin = Main.getInstance();

    public boolean isHook(){
        return plugin.getServer().getPluginManager().isPluginEnabled("MVdWPlaceholderAPI");
    }

    public boolean register(){
        if(isHook()){
            PlaceholderAPI.registerPlaceholder(plugin, "sp_player_bought_pass", new PlaceholderReplacer() {
                public String onPlaceholderReplace(PlaceholderReplaceEvent e) {
                    Player p = e.getPlayer();
                    if(p.hasPermission("seasonpass.bought")){
                        return plugin.tacc(plugin.getLang().getConfig().getString("Pass.Owned.Yes"));
                    } else return plugin.tacc(plugin.getLang().getConfig().getString("Pass.Owned.No"));
                }
            });
            return true;
        }
        return false;
    }

}
