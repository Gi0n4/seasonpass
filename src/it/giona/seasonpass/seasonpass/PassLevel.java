package it.giona.seasonpass.seasonpass;

import it.giona.seasonpass.Main;
import it.giona.seasonpass.item.ItemPack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class PassLevel {

    private ItemStack guiItem;
    private int price;
    private int level;
    private List<String> commands;
    private List<ItemPack> items;

    public PassLevel(ItemStack gui, int price, int level, List<String> commands, List<ItemPack> items){
        this.guiItem = gui;
        this.price = price;
        this.level = level;
        this.commands = commands;
        this.items = items;
    }

    public ItemStack getGuiItem(){
        return this.guiItem.clone();
    }

    public int getLevel(){
        return this.level;
    }

    public List<String> getCommands(){
        return this.commands;
    }

    public int getPrice(){
        return this.price;
    }

    public boolean canBuy(Player p){
        return !hasUnlocked(p) && Main.getInstance().getPlayerData().getStars(p) >= price;
    }

    public List<ItemPack> getItemPacks(){
        return this.items;
    }

    public boolean isNextLevel(Player p){
        return Main.getInstance().getPlayerData().getPassLevel(p) + 1 == getLevel();
    }

    public boolean hasUnlocked(Player p){
        return Main.getInstance().getPlayerData().getPassLevel(p) >= getLevel();
    }
}
