package it.giona.seasonpass.task;

import it.giona.seasonpass.Main;
import it.giona.seasonpass.seasonpass.PassLevel;
import it.giona.seasonpass.item.ItemPack;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

public class AutoLevelUp extends BukkitRunnable {
    @Override
    public void run() {
        for(Player p : Bukkit.getServer().getOnlinePlayers()){
            if(p.hasPermission("seasonpass.bought") && Main.getInstance().getConfig().getBoolean("advancement.automatic")){
                if(Main.getInstance().getPlayerData().getPassLevel(p) < Main.getInstance().getPassManager().maxLevel){
                    int nextlevel = Main.getInstance().getPlayerData().getPassLevel(p) + 1;
                    PassLevel next = Main.getInstance().getPassManager().getLevel(nextlevel);
                    if(next.canBuy(p) && hasInventorySpace(p, next)){
                        Main.getInstance().getPassManager().addLevel(p, false);
                        Main.getInstance().getPlayerData().removeStars(p, next.getPrice());
                    }
                }
            }
        }
    }

    private int getNeededSlots(Player p, PassLevel l){
        int needed = 0;
        for (ItemPack ip : l.getItemPacks()) {
            needed++;
            for (ItemStack item : p.getInventory().getStorageContents()) {
                if (item != null && item.isSimilar(ip.getItem())) {
                    if (item.getAmount() + ip.getItem().getAmount() <= ip.getItem().getMaxStackSize()) {
                        needed--;
                    }
                }
            }
        }

        return needed;
    }
    private int getFreeSlots(Player p){
        int free = 0;
        for (ItemStack item : p.getInventory().getStorageContents()) {
            if (item == null) {
                free++;
            }
        }
        return free;
    }
    private boolean hasInventorySpace(Player p, PassLevel l) {
        int needed = getNeededSlots(p, l);
        int free = getFreeSlots(p);

        return free >= needed;
    }
}