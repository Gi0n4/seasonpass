package it.giona.seasonpass.manager;

import com.comphenix.protocol.ProtocolLibrary;
import it.giona.seasonpass.Main;
import it.giona.seasonpass.manager.packets.Title;
import it.giona.seasonpass.seasonpass.PassLevel;
import it.giona.seasonpass.event.PassLevelUpEvent;
import it.giona.seasonpass.item.ItemPack;
import org.bukkit.*;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PassManager {

    private Main plugin;
    private List<PassLevel> levels;
    public int maxLevel;

    private HashMap<Player, Integer> indexes;

    public PassManager(Main p){
        this.plugin = p;
        this.levels = new ArrayList<>();
        this.indexes = new HashMap<>();
    }

    public void updateLevels(){
        levels.clear();

        for(String lvl : plugin.getPass().getConfig().getConfigurationSection("Pass.Rewards").getKeys(false)){
            try{
                int level = Integer.valueOf(lvl);
                Material m;
                try{
                    m = Material.valueOf(plugin.getPass().getConfig().getString("Pass.Rewards." + lvl + ".Material", "BARRIER"));
                }catch(IllegalArgumentException ex){
                    m = Material.BARRIER;
                }

                short data = (short) plugin.getPass().getConfig().getInt("Pass.Rewards." + lvl + ".Data", 0);

                int amount = plugin.getPass().getConfig().getInt("Pass.Rewards." + lvl + ".Amount", 1);

                int stars = plugin.getPass().getConfig().getInt("Pass.Rewards." + lvl + ".Stars");

                ItemStack item = new ItemStack(m, amount, data);
                ItemMeta meta = item.getItemMeta();

                meta.setDisplayName(plugin.getPass().getConfig().getString("Pass.Rewards." + lvl + ".Name").replaceAll("&", "§"));

                List<String> lore = plugin.getPass().getConfig().getStringList("Pass.Rewards." + lvl + ".Lore");
                for(int i=0;i<lore.size();i++){
                    lore.set(i, lore.get(i).replaceAll("%stars%", String.valueOf(stars)));
                }
                meta.setLore(plugin.taccList(lore));

                List<String> commands = plugin.getPass().getConfig().getStringList("Pass.Rewards." + lvl + ".Commands");
                item.setItemMeta(meta);

                List<String> confItems = plugin.getPass().getConfig().getStringList("Pass.Rewards." + lvl + ".Items");
                List<ItemPack> packs = new ArrayList<>();
                for(String pack : confItems){
                    if(plugin.getItemsManager().isItemPack(pack)){
                        packs.add(plugin.getItemsManager().getItemPack(pack));
                    } else {
                        plugin.getLogger().info("Cannot get the itempack " + packs + " from the item's list of level " + lvl);
                    }
                }

                levels.add(new PassLevel(item, stars, level, commands, packs));
                maxLevel++;
            }catch(NumberFormatException ex){
                plugin.getLogger().info("Unknown level: " + lvl);
            }
        }
        plugin.getLogger().info("Levels updated!");
    }

    public void addLevels(Player p, int lvl){
        for(int i = 0; i < lvl; i++){
            if(!addLevel(p, true)){
                break;
            }
        }
        p.sendMessage(plugin.getLang().getConfig().getString("Messages.Levelup.Chat.Buy").replace("&", "§").replace("%nlvl%", String.valueOf(lvl)));
        Main.getInstance().getServer().broadcastMessage(Main.getInstance().getLang().getConfig().getString("Messages.Levelup.Broadcast.Buy").replace("&", "§").replace("%nlvl%", String.valueOf(lvl)).replace("%player%", p.getName()));
    }

    public boolean isLevel(int level){
        return getLevel(level) != null;
    }

    public List<PassLevel> getLevels(){
        return levels;
    }

    public ItemStack createHeadItem(ArrowItem direction){
        ItemStack headItem = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
        SkullMeta headMeta = (SkullMeta) headItem.getItemMeta();
        headMeta.setDisplayName(direction.displayName);
        headMeta.setOwner(direction.name);
        headItem.setItemMeta(headMeta);
        return headItem;
    }

    public PassLevel getLevel(int level){
        for(PassLevel l : levels){
            if(l.getLevel() == level){
                return l;
            }
        }
        return null;
    }

    public int getPlayerIndex(Player p){
        return indexes.getOrDefault(p, 1);
    }

    private int getMaxIndex(){
        int index = getLevels().size()/7;
        if(getLevels().size() % 7 > 0){
            index++;
        }
        return index;
    }

    public Inventory getInventory(Player p, int index, boolean preview) {
        indexes.put(p, index);
        Inventory inv;
        if(!preview){
            inv = plugin.getServer().createInventory(p, 27, plugin.getPass().getConfig().getString("Pass.Name").replace("%page%", String.valueOf(index)).replaceAll("&", "§"));
        }else{
            inv = plugin.getServer().createInventory(p, 27, plugin.getPass().getConfig().getString("Pass.Name").replace("%page%", String.valueOf(index)).replaceAll("&", "§") + " §7(Preview)");
        }

        for(int i = 2; i <=8; i++){
            int level = (index-1)*7 + (i-1);

            if(index > 1) inv.setItem(plugin.getMenuManager().getSlot(1, 3), createHeadItem(ArrowItem.LEFT));
            if(index < getMaxIndex()) inv.setItem(plugin.getMenuManager().getSlot(9, 3), createHeadItem(ArrowItem.RIGHT));

            inv.setItem(plugin.getMenuManager().getSlot(i, 1), getLevel(level) != null ? getStatusItem(p, level) : new ItemStack(Material.AIR));
            inv.setItem(plugin.getMenuManager().getSlot(i, 2), getLevel(level) != null ? getLevel(level).getGuiItem() : new ItemStack(Material.AIR));
            inv.setItem(plugin.getMenuManager().getSlot(i, 3), getLevel(level) != null ? getStatusItem(p, level) : new ItemStack(Material.AIR));
        }
        return inv;
    }

    public enum ArrowItem{
        LEFT("MHF_ArrowLeft", Main.getInstance().getLang().getConfig().getString("Items.Arrows.Left").replace("&", "§")),
        RIGHT("MHF_ArrowRight", Main.getInstance().getLang().getConfig().getString("Items.Arrows.Right").replace("&", "§"));

        private String name;
        private String displayName;

        ArrowItem(String name, String displayName){
            this.name = name;
            this.displayName = displayName;
        }

        public String getOwner(){
            return this.name;
        }
    }

    public ItemStack getStatusItem(Player p, int level){

        PassLevel pl = getLevel(level);
        //int nextLevel = plugin.getPlayerData().getPassLevel(p) + 1;

        if(pl != null){

            ItemStack item;
            ItemMeta meta;

            if(pl.hasUnlocked(p)){
                item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)5);
                meta = item.getItemMeta();
                meta.setDisplayName(plugin.getLang().getConfig().getString("Items.Status.Unlocked").replaceAll("&", "§"));
            }else if(pl.canBuy(p) && pl.isNextLevel(p)){ //Sbloccabile
                item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)4);
                meta = item.getItemMeta();
                meta.setDisplayName(plugin.getLang().getConfig().getString("Items.Status.Waiting").replaceAll("&", "§"));
            }else{
                item = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short)14);
                meta = item.getItemMeta();
                meta.setDisplayName(plugin.getLang().getConfig().getString("Items.Status.Locked").replaceAll("&", "§"));
            }
            item.setItemMeta(meta);
            return item;
        } else {
            plugin.getLogger().severe("Non ho trovato alcun giocatore all'apertura del livello " + level);
            return new ItemStack(Material.BARRIER);
        }
    }

    public boolean addLevel(Player p, boolean silent){
        int level = plugin.getPlayerData().getPassLevel(p);
        PassLevel next = getLevel(level+1);
        if(next != null){
            PassLevelUpEvent event = new PassLevelUpEvent(p, level);
            plugin.getServer().getPluginManager().callEvent(event);

            if(!event.isCancelled()){
                plugin.getPlayerData().setPassLevel(p, level+1);
                if(plugin.getPlayerData().getPassLevel(p) == maxLevel){
                    Sound sound;
                    for(Player online : Bukkit.getServer().getOnlinePlayers()){
                        if(ProtocolLibrary.getProtocolManager().getProtocolVersion(online) <= 47){
                            //USING 1.8.9 or lower
                            sound = Sound.valueOf("ENDERDRAGON_GROWL");
                        }else{
                            //USING 1.9 or upper
                            sound = Sound.valueOf("ENTITY_ENDERDRAGON_GROWL");
                        }
                        online.getWorld().playSound(online.getLocation(), sound, 5.0f, 100f);
                    }

                    if(plugin.getLang().getConfig().getBoolean("Messages.MaxLevel.Broadcast.Enabled")){
                        Bukkit.broadcastMessage(plugin.tacc(plugin.getLang().getConfig().getString("Messages.MaxLevel.Broadcast.Message").replace("%player%", p.getName())));
                    }
                    if(plugin.getLang().getConfig().getBoolean("Pass.Screen.MaxLevel.Enabled")) {
                        if(ProtocolLibrary.getProtocolManager().getProtocolVersion(p) <= 47){
                            Title t = new Title(plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.MaxLevel.Title").replace("%player%", p.getName())),
                                    plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.MaxLevel.Subtitle").replace("%player%", p.getName())));
                            t.send(p);
                        }else{
                            p.sendTitle(plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.MaxLevel.Title").replace("%player%", p.getName())),
                                    plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.MaxLevel.Subtitle").replace("%player%", p.getName())));
                        }
                    }
                    if(plugin.getLang().getConfig().getBoolean("Messages.MaxLevel.Chat.Enabled")){
                        p.sendMessage(plugin.tacc(plugin.getLang().getConfig().getString("Messages.MaxLevel.Chat.Message").replace("%player%", p.getName())));
                    }
                    summonFireworks(p);
                }else{
                    if(!silent) {
                        Sound sound;
                        if(ProtocolLibrary.getProtocolManager().getProtocolVersion(p) <= 47){
                            //USING 1.8.9 or lower
                            sound = Sound.valueOf("GHAST_SCREAM");
                        }else{
                            //USING 1.9 or upper
                            sound = Sound.valueOf("ENTITY_GHAST_SCREAM");
                        }
                        p.getWorld().playSound(p.getLocation(), sound, 5.0f, 1f);

                        if (plugin.getLang().getConfig().getBoolean("Messages.Levelup.Broadcast.Enabled")) {
                            Bukkit.broadcastMessage(plugin.getLang().getConfig().getString("Messages.Levelup.Broadcast.Message").replace("%player%", p.getName()).replace("%level%", String.valueOf(level + 1)).replace("&", "§"));
                        }
                        if (plugin.getLang().getConfig().getBoolean("Pass.Screen.Levelup.Enabled")) {
                            if(ProtocolLibrary.getProtocolManager().getProtocolVersion(p) <= 47){
                                Title t = new Title(plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.Levelup.Title")
                                        .replace("%player%", p.getName()).replace("%level%", String.valueOf(level + 1))),
                                        plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.Levelup.Subtitle")
                                                .replace("%player%", p.getName()).replace("%level%", String.valueOf(level + 1))));
                                t.send(p);
                            }else{
                                p.sendTitle(plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.Levelup.Title")
                                                .replace("%player%", p.getName()).replace("%level%", String.valueOf(level + 1))),
                                        plugin.tacc(plugin.getLang().getConfig().getString("Pass.Screen.Levelup.Subtitle")
                                                .replace("%player%", p.getName()).replace("%level%", String.valueOf(level + 1))));
                            }
                        }
                        if (plugin.getLang().getConfig().getBoolean("Messages.Levelup.Chat.Enabled")) {
                            p.sendMessage(plugin.tacc(plugin.getLang().getConfig().getString("Messages.Levelup.Chat.Message")
                                    .replace("%player%", p.getName())
                                    .replace("%level%", String.valueOf(level + 1))));
                        }
                    }
                }
                for(ItemPack items : next.getItemPacks()){
                    p.getInventory().addItem(items.getItem());
                    p.updateInventory();
                }
                for(String command : next.getCommands()){
                    plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), command.replaceAll("%player%", p.getName()));
                }
                return true;
            }

        }else{
            throw new RuntimeException("Livello massimo raggiunto per il giocatore " + p.getName());
        }
        return false;
    }

    public void summonFireworks(Player p){
        Firework firework = p.getWorld().spawn(p.getLocation(), Firework.class);
        FireworkMeta fireworkMeta = firework.getFireworkMeta();
        FireworkEffect fwcolor = FireworkEffect.builder().withColor(Color.RED).withColor(Color.GREEN).withColor(Color.BLUE).withColor(Color.YELLOW).withColor(Color.PURPLE).withColor(Color.AQUA).with(FireworkEffect.Type.BALL_LARGE).build();
        fireworkMeta.addEffect(fwcolor);
        fireworkMeta.setPower(1);
        firework.setFireworkMeta(fireworkMeta);
        Sound sound;
        if(ProtocolLibrary.getProtocolManager().getProtocolVersion(p) <= 47){
            //USING 1.8.9 or lower
            sound = Sound.valueOf("FIREWORK_BLAST");
        }else{
            //USING 1.9 or upper
            sound = Sound.valueOf("ENTITY_FIREWORK_BLAST");
        }
        p.getWorld().playSound(p.getLocation(), sound, 5.0f, 100f);
    }
}
