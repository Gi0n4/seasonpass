package it.giona.seasonpass.manager;

import it.giona.seasonpass.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitTask;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class DateManager {

    private Main plugin;
    private long date;
    private String countdown, name;
    private BukkitTask task;

    public DateManager(Main plugin, String inputDate, String command, String name, String message, int index){
        this.plugin = plugin;
        this.name=name;
        try {
            this.date = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy").parse(inputDate).getTime();

            task = Bukkit.getServer().getScheduler().runTaskTimer(plugin, new Runnable() {
                @Override
                public void run() {
                    long remaining = getRemainingTime();
                    if(remaining <= 0){
                        plugin.getServer().dispatchCommand(plugin.getServer().getConsoleSender(), command);
                        Bukkit.broadcastMessage(ChatColor.translateAlternateColorCodes('&', message));
                        plugin.getTiming().getConfig().set("timings."+index+".completed", true);
                        plugin.getTiming().saveConfig();
                        cancelCount();
                        plugin.eventsUpdate();
                    }
                    countdown = getRelativeTime(remaining);
                }
            }, 0L, 20L);

        }catch(ParseException ex){
            throw new RuntimeException("Impossibile convertire la data inserita.", ex);
        }
    }

    public long getRemainingTime(){
        long time = date - System.currentTimeMillis();
        //System.out.println(time);
        return time;
    }

    public String getRelativeTime(long millis){

        long days = TimeUnit.MILLISECONDS.toDays(millis);
        millis -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(millis);
        millis -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(millis);
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(millis);

        return days + " giorni, " + hours + " ore, " + minutes + " minuti, " + seconds + " secondi";
    }

    public void cancelCount(){
        task.cancel();
    }
    public String getCountdown(){
        return this.countdown;
    }
    public String getNextEventName(){
        return this.name;
    }
}