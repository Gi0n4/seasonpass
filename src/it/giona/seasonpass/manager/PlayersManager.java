package it.giona.seasonpass.manager;

import org.bukkit.entity.Player;

import java.io.File;

public class PlayersManager extends FileManager{


    public PlayersManager(File dataFolder, String configName) {
        super(dataFolder, configName);
    }

    public boolean isRegistered(Player p){
        return getConfig().isSet("Players." + p.getUniqueId() + ".Name");
    }

    public void registerPlayer(Player p){
        getConfig().set("Players." + p.getUniqueId() + ".Name", p.getName());
        getConfig().set("Players." + p.getUniqueId() + ".Level", 0);
        getConfig().set("Players." + p.getUniqueId() + ".Stars", 0);
        saveConfig();
        reloadConfig();
    }

    public int getPassLevel(Player p){
        return getConfig().getInt("Players." + p.getUniqueId() + ".Level", 0);
    }

    public void addStars(Player p, int stars){
        getConfig().set("Players."+p.getUniqueId()+".Stars", getStars(p) + stars);
        saveConfig();
        reloadConfig();
    }
    public void setStars(Player p, int stars){
        getConfig().set("Players."+p.getUniqueId()+".Stars", stars);
        saveConfig();
        reloadConfig();
    }
    public int getStars(Player p){
        return getConfig().getInt("Players."+p.getUniqueId()+".Stars");
    }

    public void removeStars(Player p, int stars){
        setStars(p, getStars(p)-stars);
    }

    public void setPassLevel(Player p, int lvl){
        getConfig().set("Players." + p.getUniqueId() + ".Level", lvl);
        saveConfig();
        reloadConfig();
    }
}
