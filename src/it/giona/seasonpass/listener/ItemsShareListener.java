package it.giona.seasonpass.listener;

import com.comphenix.protocol.ProtocolLibrary;
import it.giona.seasonpass.Main;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemsShareListener implements Listener{

    private Main plugin;

    public ItemsShareListener(Main plugin){
        this.plugin=plugin;
    }

    @EventHandler
    public void onItemsSharing(InventoryClickEvent e){
        Player p = (Player) e.getWhoClicked();
        if(e.getCursor() == null) return;
        if(e.getInventory().getType().equals(InventoryType.CRAFTING)) return;
        if(e.getInventory().getType().equals(InventoryType.ANVIL)) return;
        List<String> invs = plugin.getConfig().getStringList("inventory.allowed");
        for(String s : invs){
            if(e.getInventory().getName().equals(s.replace("&", "§"))) return;
        }
        if(p.hasPermission("seasonpass.share")) return;
        String prevent = ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("items.locked"));
        ItemStack item = e.getCurrentItem();
        if(item == null || item.getType().equals(Material.AIR)) return;
        ItemMeta meta = item.getItemMeta();
        if(meta != null){
            List<String> lore = meta.getLore();
            if(lore != null){
                for(String s : lore){
                    if(s.equals(prevent)){
                        e.setCancelled(true);
                        break;
                    }
                }
            }
        }
    }

}
