package it.giona.seasonpass.listener;

import it.giona.seasonpass.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerListener implements Listener {

    private Main plugin;

    public PlayerListener(Main p){
        this.plugin = p;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        if(!plugin.getPlayerData().isRegistered(p)){
            plugin.getPlayerData().registerPlayer(p);
            plugin.getLogger().info("Registrazione di " + p.getName() + " avvenuta con successo!");
        }
    }
}
