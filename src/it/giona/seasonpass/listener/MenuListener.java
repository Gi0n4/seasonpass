package it.giona.seasonpass.listener;

import it.giona.seasonpass.Main;
import it.giona.seasonpass.manager.MenuManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class MenuListener implements Listener {

    private Main plugin;

    public MenuListener(Main p){
        this.plugin = p;
    }

    @EventHandler
    public void onClick(InventoryClickEvent e){

        ItemStack item = e.getCurrentItem();
        MenuManager manager = plugin.getMenuManager();

        if(e.getWhoClicked() instanceof Player){
            Player p = (Player) e.getWhoClicked();
            Inventory inv = e.getClickedInventory();
            if(inv != null){
                if(inv.getTitle() != null){
                    if(inv.getTitle().equals(plugin.getConfig().getString("Home.Name").replaceAll("&", "§"))){
                        e.setCancelled(true);
                        if(item.isSimilar(manager.createItem("Stats", p))){
                            p.closeInventory();
                        }else if(item.isSimilar(manager.createItem("Pass", p))){
                            p.openInventory(plugin.getPassManager().getInventory(p, plugin.getPassManager().getPlayerIndex(p), false));
                        }else if(item.isSimilar(manager.createItem("Challenges", p))){
                            p.closeInventory();
                            p.performCommand(plugin.getConfig().getString("Home.Items.Challenges.Command"));
                        }
                    }
                }
            }
        }
    }
}
