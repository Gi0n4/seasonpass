package it.giona.seasonpass;

import it.giona.seasonpass.listener.ItemsShareListener;
import it.giona.seasonpass.manager.*;
import it.giona.seasonpass.papi.MVDWHook;
import it.giona.seasonpass.papi.PAPIHook;
import it.giona.seasonpass.task.AutoLevelUp;
import it.giona.seasonpass.item.ItemsManager;
import it.giona.seasonpass.listener.MenuListener;
import it.giona.seasonpass.listener.PassListener;
import it.giona.seasonpass.listener.PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Main extends JavaPlugin {

    private static Main instance;
    public static Main getInstance() { return instance; }

    private FileManager seasonpass, lang, config, items, timing;
    private File playersFolder, seasonFolder;

    private MenuManager mmanager;
    private PlayersManager pmanager;
    private PassManager passManager;
    private ItemsManager itemsManager;
    private DateManager dateManager;

    private BukkitTask autoLevelUp;

    @Override
    public void onEnable() {
        instance = this;

        try {
            PluginManager pm = Bukkit.getPluginManager();
            generateFolders();
            generateFiles();
            this.pmanager = new PlayersManager(playersFolder, "players.yml");

            pm.registerEvents(new MenuListener(this), this);
            pm.registerEvents(new PlayerListener(this), this);
            pm.registerEvents(new PassListener(this), this);
            pm.registerEvents(new ItemsShareListener(this), this);

            autoLevelUp = new AutoLevelUp().runTaskTimer(this, 0L, 20L);

            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §ePlugin successfully loaded!");
        }catch (Exception e){
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §cAn error has occurred while the plugin's activation! " + e);
        }

        this.mmanager = new MenuManager(this);
        this.passManager = new PassManager(this);
        this.itemsManager = new ItemsManager(this);
        itemsManager.updateItemPacks();
        passManager.updateLevels();
        if(getServer().getPluginManager().isPluginEnabled("ProtocolLib")){
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §eHooked with ProtocolLib");
        }else{
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §cCannot find ProtocolLib, disabling plugin!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        }

        if(getServer().getPluginManager().isPluginEnabled("PlaceholderAPI")){
            new PAPIHook(this);
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §eHooked with PlaceholderAPI");
        }else{
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §cCannot find PlaceholderAPI, disabling plugin!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        }
        if(!new MVDWHook().register()){
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §cCannot find MVDWPlaceholderAPI, disabling plugin!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        }else{
            Bukkit.getServer().getConsoleSender().sendMessage("§aSeasonPass> §eHooked with MVDWPlaceholderAPI");
        }
        eventsUpdate();
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(cmd.getName().equalsIgnoreCase("pass")){

            if(args.length <= 0){
                if(sender.hasPermission("seasonpass.bought")){
                    if(sender instanceof Player){
                        Player player = (Player) sender;
                        player.openInventory(getMenuManager().getMenuInventory(player));
                    }
                }else{
                    List<String> desc = getLang().getConfig().getStringList("Pass.BuyPass");
                    for(String row : desc){
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', row));
                    }
                }
                return false;
            }else if (args.length == 1 && args[0].equalsIgnoreCase("reload") && sender.hasPermission("seasonpass.admin")) {
                Player p = (Player)sender;
                try {
                    reloadAll();
                    p.sendMessage(getLang().getConfig().getString("Messages.Technical.Reload").replace("&", "§"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }else if(args.length == 1 && args[0].equalsIgnoreCase("help")){
                Player p = (Player)sender;
                p.sendMessage("§b§m---§8§m[§7§m-§r §cSeason Pass §7§m-§8§m]§b§m---");
                p.sendMessage("§7Developers: §bGi0n4 §7& §bRhalisX99");
                p.sendMessage("§f");
                p.sendMessage("§8• §7Commands:");
                p.sendMessage("§7Help §8► §b/pass help");
                if(p.hasPermission("seasonpass.bought")){
                    p.sendMessage("§7Open §8► §b/pass");
                }else{
                    p.sendMessage("§7Informations §8► §b/pass");
                    p.sendMessage("§7Preview §8► §b/pass preview");
                }
                if(p.hasPermission("seasonpass.admin")){
                    p.sendMessage("§7Add Stars §8► §b/pass addstars <player> <amount> §c(Admin)");
                    p.sendMessage("§7Remove Stars §8► §b/pass removestars <player> <amount> §c(Admin)");
                    p.sendMessage("§7Add Levels §8► §b/pass addlevel <player> <amount> §c(Admin)");
                    p.sendMessage("§7Reload Plugin §8► §b/pass reload §c(Admin)");
                }
                p.sendMessage("§b§m---§8§m[§7§m-§r §cSeason Pass §7§m-§8§m]§b§m---");
            }else if(args.length==3 && args[0].equalsIgnoreCase("addstars")) {
                if(sender.hasPermission("seasonpass.admin")){
                    Player target = getServer().getPlayer(args[1]);
                    int stars = Integer.parseInt(args[2]);
                    if (target != null && stars > 0) {
                        getPlayerData().addStars(target, stars);
                        sender.sendMessage(getLang().getConfig().getString("Messages.Technical.Stars.Added").replace("&", "§").replace("%player%", target.getName()).replace("%stars%", String.valueOf(stars)));
                    } else {
                        sender.sendMessage("§cSyntax error: §e/pass addstars <player> <amount> §7- §4[!] §7amount > 0");
                    }
                }else{
                    sender.sendMessage(getLang().getConfig().getString("Messages.Error.NoPermission").replace("&", "§"));
                }
            }else if(args.length==3 && args[0].equalsIgnoreCase("removestars")) {
                if(sender.hasPermission("seasonpass.admin")) {
                    Player target = getServer().getPlayer(args[1]);
                    int stars = Integer.parseInt(args[2]);
                    if (target != null && stars > 0) {
                        getPlayerData().removeStars(target, stars);
                        sender.sendMessage(getLang().getConfig().getString("Messages.Technical.Stars.Removed").replace("&", "§").replace("%player%", target.getName()).replace("%stars%", String.valueOf(stars)));
                    } else {
                        sender.sendMessage("§cSyntax error: §e/pass removestars <player> <amount> §7- §4[!] §7amount > 0");
                    }
                }else{
                    sender.sendMessage(getLang().getConfig().getString("Messages.Error.NoPermission").replace("&", "§"));
                }
            }else if(args.length == 3 && args[0].equalsIgnoreCase("addlevel")){
                if(sender instanceof ConsoleCommandSender) {
                    Player target = getServer().getPlayer(args[1]);
                    int lvl = Integer.parseInt(args[2]);
                    if(target != null && lvl > 0){
                        getPassManager().addLevels(target, lvl);
                        sender.sendMessage(getLang().getConfig().getString("Messages.Technical.Level.Added").replace("&", "§").replace("%player%", target.getName()).replace("%levels%", String.valueOf(lvl)));
                    }else{
                        sender.sendMessage("§cSyntax error: §epass addlevel <player> <amount> §7- §4[!] §7amount > 0");
                    }
                }else{
                    sender.sendMessage(getLang().getConfig().getString("Messages.Error.OnlyConsole").replace("&", "§"));
                }
            }else if(args.length == 1 && args[0].equalsIgnoreCase("preview")){
                if(sender instanceof Player){
                    Player p = (Player)sender;
                    if(!p.hasPermission("seasonpass.bought") || p.hasPermission("seasonpass.admin")){
                        p.openInventory(getPassManager().getInventory(p, 1, true));
                    }else{
                        p.sendMessage(getLang().getConfig().getString("Messages.Error.NoPermission").replace("&", "§"));
                    }
                }
            }else{
                sender.sendMessage("§cCommand not found!");
            }
        }
        return true;
    }

    private void generateFolders(){

        //Generate the players folder
        playersFolder = new File(getDataFolder().toString() + "/Players");
        if(!playersFolder.exists()){
            playersFolder.mkdir();
        }

        //Generate the rewards folder
        seasonFolder = new File(getDataFolder().toString() + "/Pass");
        if(!seasonFolder.exists()){
            seasonFolder.mkdir();
        }
    }

    private void generateFiles(){
        lang = new FileManager(getDataFolder(), "lang.yml");
        seasonpass = new FileManager(seasonFolder, "seasonpass.yml");
        config = new FileManager(getDataFolder(), "config.yml");
        items = new FileManager(seasonFolder, "CustomItems.yml");
        timing = new FileManager(seasonFolder, "timing.yml");
    }

    public void reloadAll(){
        pmanager.reloadConfig();
        seasonpass.reloadConfig();
        lang.reloadConfig();
        config.reloadConfig();
        items.reloadConfig();
        passManager.updateLevels();
        getServer().getPluginManager().disablePlugin(this);
        getServer().getPluginManager().enablePlugin(this);
        eventsUpdate();
    }

    public PlayersManager getPlayerData(){
        return this.pmanager;
    }
    public FileManager getItems(){ return  this.items; }
    public FileManager getLang(){
        return this.lang;
    }
    public FileManager getPass(){
        return this.seasonpass;
    }
    public FileManager getTiming(){
        return this.timing;
    }
    public FileManager getConfiguration(){
        return this.config;
    }
    public PassManager getPassManager(){
        return this.passManager;
    }
    public ItemsManager getItemsManager(){
        return this.itemsManager;
    }

    @Override
    public FileConfiguration getConfig(){
        return getConfiguration().getConfig();
    }

    public String tacc(String input){
        if(input != null){
            return ChatColor.translateAlternateColorCodes('&', input);
        } else return null;
    }

    public List<String> taccList(List<String> lores){
        List<String> newLores = new ArrayList<>();
        for (String s : lores) {
            newLores.add(ChatColor.translateAlternateColorCodes('&', s));
        }
        return newLores;
    }

    public MenuManager getMenuManager(){
        return this.mmanager;
    }
    public DateManager getDateManager(){
        return this.dateManager;
    }

    public void eventsUpdate(){
        int size = getTiming().getConfig().getConfigurationSection("timings").getKeys(false).size();
        int i=0;
        for(String ne : getTiming().getConfig().getConfigurationSection("timings").getKeys(false)){
            if(i<size){
                if(!getTiming().getConfig().getBoolean("timings."+ne+".completed")){
                    dateManager = new DateManager(this, getTiming().getConfig().getString("timings."+ne+".date"),
                            getTiming().getConfig().getString("timings."+ne+".command"),
                            getTiming().getConfig().getString("timings."+ne+".name"),
                            getTiming().getConfig().getString("timings."+ne+".broadcast.message"),
                            Integer.parseInt(ne));
                    break;
                }else{
                    i++;
                }
            }else{
                break;
            }
        }
    }
}
